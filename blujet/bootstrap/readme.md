| OS        | Windows
| ------------- |-------------

# Bootstrap
Bootstrapping the development environment involves installing the basic artifacts that are in turn used to establish the complete development environment.
## SRE Strategy
The SRE strategy is currently defined and tested only for the Windows platform.  The following table describes various requirements along with the assumed default values. 

| Attribute        | Default|Description|
|:-----------------|:-------|:-------------
|Install path      |%HOMEDRIVE%\blujet\programs|Parent directory of all artifacts installed by the BluJet strategy
|Download directory|%USERPROFILE%\Downloads|The user download directory

# Artifacts
The folllowing instructions are used to setup the necessary artifacts to bootstrap.
### Chef Knife Client
1. Download the [Windows Chef Client](https://packages.chef.io/files/stable/chef/12.19.36/windows/2012/chef-client-12.19.36-1-x64.msi).  By default, the browser should store the the artifact in the user downloads directory.  If the installer is downloaded anywhere else, substitute the location in the following step.
2. Install the Chef Client by executing the following command in a shell.

    ```sh
    $ msiexec /qb! /i %HOMEDRIVE%%HOMEPATH%\Downloads\chef-client-12.19.36-1-x64.msi ADDLOCAL="ChefClientFeature"
    ```

### JDK 8.X
Because of license restrictions, the JDK cannot be hosted on a public artifact repository.  Acceptance of the JDK license must be manually accepted. The JDK install image must then be manually downloaded to the target machine.  From the download location, the respective recipe will install the JDK.
1. Download the [JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).  Take note if the JDK was not installed to the default downloads directory. In this case, the attributes for the recipe must be udpated.

### Git
1. Download the [Git client](https://github.com/git-for-windows/git/releases/download/v2.12.2.windows.1/Git-2.12.2-64-bit.exe) to the target machine.  The default download location is recommended.
2. Create and copy the [git-configuration.ini ](https://gitlab.com/armin.garcia/devops/blob/master/blujet/bootstrap/git-configuration.ini) to the local machine.  For the purpose of these instructions, we will assume the default download directory.  
3. In the git-configuration.ini file, the DIR entry must reflect the Install path. Edit the entry as needed.  For example:
 
        ...
        Dir=C:\blujet\programs\Git
        ...
4. From the command line and from the download directory, install git with the following command:
    
    ```sh
    $ .\Git-2.12.2-64-bit.exe /SILENT /LOADINF="git-configuration.ini"
    ```
