default['artifact']['id'] = 'apache-ant'
default['artifact']['groupId'] = "org.apache"
default['artifact']['version'] = '1.9.4'

default['artifact']['installer']['binary'] = "http://mirror.tcpdiag.net/apache//ant/binaries/apache-ant-1.9.4-bin.zip"
default['artifact']['installer']['zip']['collapsed_ancestor_levels']=1