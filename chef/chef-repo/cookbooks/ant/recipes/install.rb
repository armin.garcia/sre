#
# Author:: Armin M. Garcia
# Cookbook Name:: ant
#
# Copyright:: 2014, BluJet IO, Inc


artifact_environment "ANT_HOME" do
	value "node['artifact']['installer']['install_to']"
	action :nothing
end

artifact_environment "PATH" do
	value "node['artifact']['installer']['install_to']+'/bin'"
	action :nothing
end

artifact_installer "apache-ant" do
	 notifies :update, "artifact_environment[PATH]", :immediately
	 notifies :set   , "artifact_environment[ANT_HOME]", :immediately
end