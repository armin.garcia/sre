# Organization
| Attribute     | Description                                |Default          | 
| ------------- |--------------------------------------------|-----------------|
| Name          | The name of the organization.              |Blu Jet IO, Inc.|
| Namespace     | The namespace assigned to the organization.|io.blujet        |

# Project
| Attribute     | Description                                                          | 
| ------------- |----------------------------------------------------------------------|
| Name          | The name of the project.                                             |
| Repository  | A list of artifact repositories used to store and retrieve artifacts.|
| Local Repository| A location on the local machine where the artifacts will be stored.| 

# Artifact Attributes
| Attribute       | Description                                                        |Defaults To                                 |Example                        |
| ----------------|--------------------------------------------------------------------|--------------------------------------------|-------------------------------|
| Name            | A globally unique and fully qualified identifier for the artifact .|$groupId.$id-$version-$classifier.$packaging|io.blujet.ws-0.0.1-snapshot    |
| Unqualified Name| The unqualified artifact name.                                     |$id-$version-$classifier                    |ws-0.0.1-snapshot              |
| GroupId         | The namespace for the artifact.                                    |Organization namespace                      |io.blujet                      |
| Id              | The logical name of the artifact.                                  |Chef resource name                          |ws                             |
| Version         | The version of the artifact.                                       |0.0.1                                       |0.0.1                          |
| Classifier      | A further classification of the artifact.                          |                                            |snapshot                       |
| Packaging       | A description of how the artifact is packaged                      |                                            |war                            |
| Download URI    | A specific location that stores the artifact.                      |                                            |https://artifacts.blujet.io/syz|

# Artifact Installer
| Attribute       | Description                                                        |Defaults To                                 |
| ----------------|--------------------------------------------------------------------|--------------------------------------------|
| Install To      | The directory where the artifact will be installed to.             |  
| Environment     | A list of environment                                              |


Use Cases:  install, uninstall, installed?

# Artifact Environment Variable
| Attribute     | Description                                                        |Default|
| ------------- |--------------------------------------------------------------------|:-----:|
| Variable      | The name of the environment variable.                              |       |
| Value         | A globally unique and fully qualified identifier for the artifact .|       |
| Scope         | The scope of the environment variable:  user or system.            |system |

Use Cases:  set, unset 
