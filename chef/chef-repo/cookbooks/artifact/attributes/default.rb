
# include_attribute "organization"

default['artifact']['local_repository'] = "#{ENV['SYSTEMDRIVE']}/blujet/artifacts"   if platform?("windows")
default['artifact']['programs_dir'] = "#{ENV['SYSTEMDRIVE']}/blujet/programs"        if platform?("windows")
default['artifact']['installer']['owner'] = Etc.getlogin