
require 'uri'
require 'Win32API' if Chef::Platform.windows?
require 'chef/exceptions'
require 'pathname'
require 'zip'

module Artifact
  module File
    def normalize_to_windows(file)
      file.split(::File::SEPARATOR).join('\\')
    end
  end

  module Attribute

    def versioned_id
        return node['artifact']['versioned_id']     if node['artifact']['versioned_id'] != nil
        node.default['artifact']['versioned_id'] = "#{@new_resource.id}-#{@new_resource.version}"
        node['artifact']['versioned_id'] 
    end

    def classified_id
      classified_id = versioned_id
      classified_id +=  "-" + @new_resource.classifier    if @new_resource.classifier != nil  
      classified_id
    end

    def packaging(node)
       return node['artifact']['packaging']     if node['artifact']['packaging'] != nil

       if node['artifact']['installer']['binary'] != nil 
           node['artifact']['installer']['binary'] =~ /.*\.([^.]+)$/
           node.default['artifact']['packaging'] = Regexp.last_match(1)
       end

       node['artifact']['packaging']
    end

    def binary_name(node)
       packaging(node) != nil ? classified_id+"."+packaging(node) : classified_id
    end

    def local_repository_binary(node)
       return node['artifact']['local_repository_binary']     if node['artifact']['local_repository_binary'] != nil

       local_repository_binary = @new_resource.local_repository + "/"+ @new_resource.groupId.tr('.','/')+"/"+@new_resource.id+"/"
       local_repository_binary += @new_resource.version+"/"+binary_name(node)
       node.default['artifact']['local_repository_binary'] = local_repository_binary
       node.default['artifact']['local_repository_binary']
    end
     
    def new_path(current_path, target_directory, collapse_ancestor_levels)
      ## If there is no directory in the current path, return the regular filename
      return current_path                              if current_path !~ /\// || collapse_ancestor_levels == 0

      ## Split the filename into parts
      parts = current_path.split('/')
      
      ## If the current path is a directory and is being collapsed, return nil
      # return nil                                       if parts[collapse_ancestor_levels] == ""

      ## Remove the ancestor directories
      # parts.shift(collapse_ancestor_levels)
      parts.shift(collapse_ancestor_levels)

      ## If a target directory is specified, prepend it
      parts.unshift(target_directory)                  if ! target_directory.to_s.empty?

      ## Join the remaining parts
      parts.join('/')      
    end

    def install_to(node)
       return node['artifact']['installer']['install_to']     if node['artifact']['installer']['install_to']  != nil

       # node.default['artifact']['installer']['install_to'] = node['artifact']['programs_dir']+"/"+node['artifact']['id'] +"-"+node['artifact']['version']
       node.default['artifact']['installer']['install_to'] = node['artifact']['programs_dir']+"/"+versioned_id
 
       node['artifact']['installer']['install_to']
    end

    def unzip(zipped_file,target_directory,collapse_ancestor_levels)
      Zip::File.open(zipped_file) do |zip|        
        zip.each do |entry|                    
          path = new_path(entry.to_s, target_directory, collapse_ancestor_levels)
          FileUtils.mkdir_p(::File.dirname(path))
          FileUtils.rm(path)                           if ::File.exists?(path) && !::File.directory?(path)
          zip.extract(entry, path)
          new_resource.updated_by_last_action(true)
        end
      end
    end
  end
end

Chef::Recipe.send(:include, Artifact::Attribute)