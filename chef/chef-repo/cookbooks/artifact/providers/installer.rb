#
# Author:: Armin M. Garcia (<armin@blujet.io>)
# Cookbook Name:: artifact
# Provider:: installer
#

use_inline_resources if defined?(use_inline_resources)

include Artifact::Attribute

action :install do
	local_binary = local_repository_binary(node)
    packaging = packaging(node)
    target_directory = install_to(node)
    
	directory ::File.dirname(local_binary) do
	  recursive true
	  action :create
    end

	remote_file local_binary do
	  source new_resource.installer['binary']
	  action :create_if_missing
	end

	artifact_zip local_binary do
	  target_directory target_directory
	  collapsed_ancestor_levels node['artifact']['installer']['zip']['collapsed_ancestor_levels']
	end	if packaging == "zip" 

	execute local_binary do
	  cwd ::File.dirname(local_binary)
	  command ::File.basename(local_binary)+"  "+node['artifact']['installer']['exe']['options']
	end if packaging == "exe" 

    windows_package node['artifact']['installer']['msi']['package_name'] do
       source local_binary
       options node['artifact']['installer']['msi']['options']
       installer_type :msi
       action :install
    end if packaging == "msi" 
end

action :remove do
end
