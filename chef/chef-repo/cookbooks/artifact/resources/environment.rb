#
# Author:: Armin M. Garcia
# Cookbook Name:: artifact
# Resource:: path
#
# Copyright:: 2014, BluJet IO, Inc


default_action :set
actions :set, :update, :unset, :nothing

attribute :name, :kind_of => String, :name_attribute => true
attribute :value, :kind_of => String
