#
# Author:: Armin M. Garcia
# Cookbook Name:: artifact
# Resource:: artifact
#
# Copyright:: 2014, BluJet IO, Inc


default_action :install
actions :install

attribute :id, :kind_of => String, :default => node['artifact']['id'] 
attribute :groupId, :kind_of => String, :default => node['artifact']['groupId'] 
attribute :version, :kind_of => String, :default => node['artifact']['version'] 
attribute :packaging, :kind_of => String, :default => node['artifact']['packaging'] 
attribute :classifier, :kind_of => String, :default => node['artifact']['classifier'] 
attribute :local_repository, :kind_of => String, :default => "#{ENV['SYSTEMDRIVE']}/blujet/artifacts"
attribute :installer, :default => node['artifact']['installer'] 