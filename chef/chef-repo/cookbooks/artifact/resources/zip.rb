#
# Author:: Armin M. Garcia
# Cookbook Name:: artifact
# Resource:: artifact
#
# Copyright:: 2014, BluJet IO, Inc


default_action :unzip
actions :unzip, :nothing

attribute :zipfile, :kind_of => String, :name_attribute => true
attribute :target_directory, :kind_of => String, :required => true
attribute :collapsed_ancestor_levels, :kind_of => Integer, :default => 0
