default['artifact']['id'] = 'cygwin'
default['artifact']['groupId'] = 'com.cygwin'
default['artifact']['version'] = '6.3'
default['artifact']['versioned_id'] = default['artifact']['id'] +"-"+default['artifact']['version']

default['cygwin']['respository'] = "http://mirrors.kernel.org/sourceware/cygwin"
default['cygwin']['packages'] = ['autossh', 'bash', 'bash-completion', 'bashdb', 'chere', 'cygutils', 'findutils', 'gzip', 'gvim', 'hostname', 'httperf', 'httping', 'httptunnel', 'less', 'links', 'man-db', 'mintty', 'ncurses', 'openssh', 'openssl', 'ping', 'rsync', 'screen', 'sed', 'tar', 'tcsh', 'time', 'unzip', 'wget', 'which', 'whois', 'zip', 'grep', 'gawk'].join(",")

default['artifact']['installer']['binary'] = "http://cygwin.com/setup-x86_64.exe"
default['artifact']['installer']['install_to'] = default['artifact']['programs_dir']+"/"+default['artifact']['versioned_id']
default['artifact']['installer']['exe']['options']="-q -O -R \""+node['artifact']['installer']['install_to'].tr('/','\\') +"\" -s #{node['cygwin']['respository']} -P \"#{node['cygwin']['packages']}\""