name             'cygwin'
maintainer       'Ryan Larson & Ben Jansen'
maintainer_email 'aogail@w007.org'
license          'Apache 2.0'
description      'Installs/Configures cygwin'
long_description 'Installs/Configures cygwin'
version          '0.4.5'

supports 'windows'

depends 'artifact'
# depends 'windows'
# depends 'windows_firewall'
# depends 'artifact'

