artifact_environment "PATH" do
	value "node['artifact']['installer']['install_to']+'/bin'"
	action :nothing
end

artifact_installer "jdk" do
	 notifies :update, "artifact_environment[PATH]", :immediately
end