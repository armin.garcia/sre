default['artifact']['id'] = 'eclipse-java'
default['artifact']['groupId'] = "org.eclipse"
default['artifact']['version'] = 'luna-SR1-RC3'
default['artifact']['classifier'] = 'win32-x86_64'

default['artifact']['installer']['binary'] = "https://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/luna/SR1-RC3/eclipse-java-luna-SR1-RC3-win32-x86_64.zip"
default['artifact']['installer']['zip']['collapsed_ancestor_levels']=1
