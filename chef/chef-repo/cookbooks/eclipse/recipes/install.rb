#
# Author:: Armin M. Garcia
# Cookbook Name:: ant
#
# Copyright:: 2014, BluJet IO, Inc


artifact_environment "PATH" do
	value "node['artifact']['installer']['install_to']"
	action :nothing
end

artifact_installer "eclipse" do
	 notifies :update, "artifact_environment[PATH]", :immediately
end
