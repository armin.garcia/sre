default['artifact']['id'] = 'git-tortoise'
default['artifact']['groupId'] = "org.tortoise"
default['artifact']['version'] = '1.8.8.0'
default['artifact']['classifier'] = '64bit'

default['artifact']['installer']['binary'] = "http://download.tortoisegit.org/tgit/1.8.12.0/TortoiseGit-1.8.12.0-64bit.msi"
default['artifact']['installer']['msi']['package_name']="TortoiseGit 1.8.8.0 (64 bit)"
default['artifact']['installer']['msi']['options']="/norestart"
