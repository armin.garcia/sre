default['artifact']['id'] = 'jdk'
default['artifact']['groupId'] = 'com.oracle'
default['artifact']['version'] = '1.7.0_71'
default['artifact']['classifier'] = 'windows-x64'
default['artifact']['versioned_id'] = "#{default['artifact']['id']}#{default['artifact']['version']}"

default['artifact']['installer']['binary'] = "file:///#{ENV['SYSTEMDRIVE']}/Users/#{default['artifact']['installer']['owner']}/Downloads/jdk-7u71-windows-x64.exe"
default['artifact']['installer']['exe']['options']=' /v "/qn'

default['artifact']['programs_dir'] = "#{ENV['SYSTEMDRIVE']}/Program Files/Java"        if platform?("windows")