#
# Author:: Armin M. Garcia
# Cookbook Name:: jdk
#
# Copyright:: 2014, BluJet IO, Inc

artifact_environment "JAVA_HOME" do
	value "node['artifact']['installer']['install_to']"
	action :nothing
end

artifact_environment "PATH" do
	value "node['artifact']['installer']['install_to']+'/bin'"
	action :nothing
end

artifact_installer "jdk" do
	 notifies :update, "artifact_environment[PATH]", :immediately
	 notifies :set   , "artifact_environment[JAVA_HOME]", :immediately
end