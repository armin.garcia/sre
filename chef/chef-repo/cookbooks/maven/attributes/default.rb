default['artifact']['id'] = 'apache-maven'
default['artifact']['groupId'] = "org.apache"
default['artifact']['version'] = '3.2.5'

default['artifact']['installer']['binary'] = 'http://apache.claz.org/maven/maven-3/3.2.5/binaries/apache-maven-3.2.5-bin.zip'
default['artifact']['installer']['zip']['collapsed_ancestor_levels']=1