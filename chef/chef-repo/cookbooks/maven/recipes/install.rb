#
# Author:: Armin M. Garcia
# Cookbook Name:: ant
#
# Copyright:: 2014, BluJet IO, Inc


# maven_installer "apache-maven" do
# end

artifact_environment "MAVEN_HOME" do
	value "node['artifact']['installer']['install_to']"
	action :nothing
end

artifact_environment "PATH" do
	value "node['artifact']['installer']['install_to']+'/bin'"
	action :nothing
end

artifact_installer "apache-maven" do
	 notifies :update, "artifact_environment[PATH]"
	 notifies :set   , "artifact_environment[MAVEN_HOME]"
end