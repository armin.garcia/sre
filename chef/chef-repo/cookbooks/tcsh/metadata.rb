name             'tcsh'
maintainer       'YOUR_NAME'
maintainer_email 'YOUR_EMAIL'
license          'All rights reserved'
description      'Installs/Configures tcsh'
long_description 'Installs/Configures tcsh'

version          '0.0.1

depends          'cygwin'
