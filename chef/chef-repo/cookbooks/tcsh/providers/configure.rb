
use_inline_resources if defined?(use_inline_resources)

include Artifact::Attribute

action :configure do 
# Chef::Log.info(node)

	windows_shortcut "tcsh.lnk" do
  		name Chef::Config.platform_specific_path(node['tcsh']['shortcut'])
  		target Chef::Config.platform_specific_path(node['cygwin']['mintty'])
  		arguments "/bin/tcsh"
  		description "The tcsh shell"
  		action :create
	end	

end
