#
# Author:: Armin M. Garcia
# Cookbook Name:: tcsh
# Resource:: path
#
# Copyright:: 2014, BlueJet IO, Inc

default_action :configure
actions :configure

# attr_accessor :download_uri

# attribute :path, :kind_of => String, :name_attribute => true
# attribute :name, :kind_of => String
# attribute :groupId, :kind_of => String, :required => true
# attribute :id, :kind_of => String
# attribute :version, :kind_of => String, :default => '0.0.1'
# attribute :classifier, :kind_of => String
# attribute :packaging, :kind_of => String, :required => true
# attribute :download_uri, :regex => URI.regexp , :default => nil