name "development"
description "An environment used for development"

default_attributes(
  "java" => {
    "install_flavor" => "windows",
    "jdk_version" => "7",
    "windows" => { 
      "url" =>  "file:///C:/Users/armin/Downloads/jdk-7u71-windows-x64.exe"
    },
  "eclipse" => {
      "version" => "luna",
      "release_code" => "R",
      "arch" => "x64_86",
      "suite" => "java"
     }
  }
)
override_attributes ({
})
